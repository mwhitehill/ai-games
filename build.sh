#!/usr/bin/env bash

echo 'compiling'
mkdir bin
cd src
javac -d ../bin *.java
echo 'done compiling'

echo 'running using test engine'
cd ../bin
java Bot test_with_my_engine
echo 'done running test'

echo 'cleaning'
cd ..
rm -r bin
rm -r .DS_Store
echo 'done cleaning'

echo 'zipping source files'
cd src
zip Archive.zip *.java
echo 'done zipping source files'