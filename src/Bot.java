
import java.util.Scanner;

public class Bot {
	private Match myMatch;

	private int testCol, testRow;

	public Bot() {
		myMatch = new Match();
	}

	public void listen() {
		Scanner sc = new Scanner(System.in);
		while (sc.hasNextLine()) {
			parseCommand(sc, null);
		}
		sc.close();
	}

	private void parseCommand(Scanner sc, Engine engine) {
		boolean useMyEngine = engine != null;

		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			if (line.length() == 0) { continue; }
			String[] parts = line.split(" ");
			switch(parts[0]) {
					case "settings":
							switch (parts[1]) {
								case "timebank":
									myMatch.setTimeBank(new Integer(parts[2]).intValue());
									break;
								case "time_per_move":
									myMatch.setTimePerMove(new Integer(parts[2]).intValue());
									break;
								case "player_names":
									String[] names = parts[2].split(",");
									myMatch.setMyPlayerName(names[0]);
									myMatch.setTheirPlayerName(names[1]);
									break;
								case "your_bot":
									// make sure the player names are assigned correctly
									if (!myMatch.getMyPlayerName().equals(parts[2])) {
										String temp = myMatch.getMyPlayerName();
										myMatch.setMyPlayerName(parts[2]);
										myMatch.setTheirPlayerName(temp);
									}
									break;
								case "your_botid":
									myMatch.setMyBotID(new Integer(parts[2]).intValue());
									break;
								default:
									System.err.println("failed to parse command");
							}
							break;

					case "update":
							switch (parts[2]) {
								case "round":
									myMatch.setCurrentRound(new Integer(parts[3]).intValue());
									break;
								case "move":
									myMatch.setCurrentMove(new Integer(parts[3]).intValue());
									break;
								case "field":
									myMatch.setCurrentBoard(parts[3]);
									System.err.println(myMatch.getCurrentBoard());
									break;
								case "macroboard":
									myMatch.setCurrentMacroBoard(parts[3]);
									System.err.println(myMatch.getCurrentMacroBoard());
									break;
								default:
									System.err.println("failed to parse command");
							}
							break;

					case "action":
						// if testing
						if (useMyEngine) {
							sendMoveMessage(new Move(testRow++, testCol++, 1), engine);
							Action.getMove(myMatch);
						}
						// if in competition
						else {
							System.out.println(new Move((int)(Math.random() * 9), (int)(Math.random() * 9), 1).formatForEngine());
						}
						break;
					default:
						System.err.println("failed to parse command");
			}
		}
	}

	// new, added for test engine to work
	public void handleMessage(String message, Engine engine) {
		parseCommand(new Scanner(message), engine);
	}

	//new
	public void sendMoveMessage(Move move, Engine engine) {
		engine.handleMoveMessage(move);
	}

	public static void main(String[] args) {

		if (args.length > 0 && args[0].equals("test_with_my_engine")) {
			

			Engine testEngine = new Engine(new Bot());
			testEngine.run();
		}
		else {
			new Bot().listen();
		}
	}

}
