public class Board {
    private int[][] board;

    /**
     * Create a board from a csv String of values
     * @param boardVals
     * 			the csv String to decode
     */
    public Board(String boardVals) {
        String[] field = boardVals.split(",");

        int rows, cols;
        rows = cols = (int)(Math.sqrt(field.length));
        board = new int[rows][cols];

        int fieldCounter = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = Integer.parseInt(field[fieldCounter++]);
            }
        }
    }

    /**
     * Copy constructor for Board
     * @param other
     * 			the board to copy
     */
    public Board(Board other) {
        board = new int[other.board.length][other.board[0].length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = other.board[i][j];;
            }
        }
    }


    public int get(int row, int col) {
        return board[row][col];
    }

    public void set(int val, int row, int col) {
        board[row][col] = val;
    }

    public int getRows() {
        return board.length;
    }

    public int getCols() {
        return board[0].length;
    }

    public String toString() {
        String str = "";
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                str += board[i][j] + " ";
            }
            str += "\n";
        }

        return str;
    }

    public String toCSV() {
      String str = "";

      for (int i = 0; i < board.length; i++) {
        for (int j = 0; j < board[i].length; j++) {
          if (i == board.length - 1 && j == board[i].length - 1) {
            str += board[i][j];
          }
          else {
            str += board[i][j] + ",";
          }
        }
      }

      return str;
    }

    // returns true if boards are identical
    public boolean equals(Object other) {
        if (!(other instanceof Board)) {
            return false;
        }

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (!(((Board)other).get(i, j) == board[i][j])) {
                    return false;
                }
            }
        }

        return true;
    }
    public void move (Move move){
        board [move.getRow()][move.getCol()]=move.getPerson();
    }

    public Move getLastMove(Board last, Match game){
        Move perviouse;
        for(int row=0; row<board.length; row++){
            for(int column=0; column<board[row].length; column++ ){
                if(board[row][column]==last.get(row, column)){
                    perviouse=new Move(row, column, board[row][column]);
                    return perviouse;
                }

            }
        }
        System.out.println("NO RECENT MOVE!!!!");
        System.exit(-1);
        return null;

    }

    //Takes an index (Row, Col) of a macro board and returns the micro board of it.
    public Board GetSubboard(int y, int x){
        if(y==-1||x==-1){
            System.out.println("Subboard Failed");
            System.exit(-1);
        }

        String boardconstructor="";

        for(int row=y*3; row<(y*3)+3; row++){
            for(int col=x*3; col<(x*3)+3; col++){
                boardconstructor+=board[row][col];
            }
        }
        Board micro=new Board(boardconstructor);
        return micro;
    }

}
