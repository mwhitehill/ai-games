
public class Move {
	private int row;
	private int col;
	private double score;
	private int botID;

	/**
	 * Construct a new Move object
	 * @param row
	 * 		the row for this Move
	 * @param col
	 * 		the column for this Move
	 * @param botID
	 * 		the ID of the bot who made/is making this Move
     */
	public Move(int row, int col, int botID) {
		this.row = row;
		this.col = col;
		this.botID = botID;
	}

	/**
	 * Format this Move into a String readable by the engine
	 * @return this Move as a String formatted for the engine
     */
	public String formatForEngine() {
		return String.format("place_move %d %d", col, row);
	}

	/**
	 * Get the 0 based index of this Move's row
	 * @return this Move's current row value
     */
	public int getRow() {
		return row;
	}

	/**
	 * Set this Move's row
	 * @param row
	 * 		this Move's new row value
     */
	public void setRow(int row) {
		this.row = row;
	}

	/**
	 * Get the 0 based index of this Move's column
	 * @return this Move's current column value
	 */
	public int getCol() {
		return col;
	}

	/**
	 * Set this Move's column
	 * @param row
	 * 		this Move's new column value
	 */
	public void setCol(int col) {
		this.col = col;
	}

	/**
	 * Get the score for this Move
	 * Score is determined by how valuable the move is and is updated when this method is called
	 * @return
	 * 		the score of this Move
     */
	public double getScore() {
		// TODO: return Action.getScore(this)
		return score;
	}

	/**
	 * Get the id of the bot who made the move
	 * @return
	 *		the botID that made this move (1 or 2)
     */
	public int getPerson() {
		return botID;
	}



	//this is pretty primative, we may want to change it later
	public String toString(){
		return"("+row+", "+col+")";
	}

}
