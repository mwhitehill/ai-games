import java.io.File;
import java.util.Scanner;

public class Engine {

  private Bot testBot;
  private Board gameBoard;
  private Board macroBoard;
  private int roundNum;
  private int moveNum;

  Scanner stdIn;

  public Engine(Bot testBot) {
    this.testBot = testBot;

    // load initial game boards
    gameBoard = new Board("0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0");
    macroBoard = new Board("-1,-1,-1,-1,-1,-1,-1,-1,-1");

    stdIn = new Scanner(System.in);
  }

  public void run() {
    roundNum = 1;
    moveNum = 1;

    // send settings message given in example
    sendMessage("settings timebank 10000\n" +
      "settings time_per_move 500\n" +
      "settings player_names player1,player2\n" +
      "settings your_bot player1\n" +
      "settings your_botid 1\n");

      for (int i = 0; i < 100; i++) {
        sendMessage("update game round " + roundNum + "\n" +
        "update game move " + moveNum + "\n" +
        "update game field " + gameBoard.toCSV() + "\n" +
        "update game macroboard " + macroBoard.toCSV() + "\n" +
        "action move 10000");
      }

  }


  public void sendMessage(String message) {
    testBot.handleMessage(message, this);
  }

  public void handleMoveMessage(Move move) {
    // if the bots move was valid
    if (macroBoard.get(move.getRow() / 3, move.getCol() / 3) == -1
      && gameBoard.get(move.getRow(), move.getCol()) != 2) {
      // restore the macroboard
      restoreMacroBoard();
      gameBoard.set(1, move.getRow(), move.getCol()); //assume player1 is the bot
      // update the macroboard
      macroBoard.set(-1, move.getRow() % 3, move.getCol() % 3);
      //print bot move
      System.err.println("Move made by bot: \n" + gameBoard + "\n" + macroBoard);
    }
    else {
      // otherwise, don't accept the players move and make the macroboard empty
      clearMacroBoard();
      System.err.println("Move by bot invalid");
    }
    moveNum++;

    // get user input, row and column
    String[] rowCol = stdIn.nextLine().split(" ");
    int row = new Integer(rowCol[0]).intValue();
    int col = new Integer(rowCol[1]).intValue();

    if (macroBoard.get(row / 3, col / 3) == -1
    && gameBoard.get(row, col) != 1) {
      restoreMacroBoard();
      gameBoard.set(2, row, col);
      macroBoard.set(-1, row % 3, col % 3);
    }
    else {
      clearMacroBoard();
      System.out.println("Move by user invalid");
    }
    roundNum++;
  }

  // allows any move
  private void clearMacroBoard() {
    for (int i = 0; i < macroBoard.getRows(); i++) {
      for (int j = 0; j < macroBoard.getCols(); j++) {
        macroBoard.set(-1, i, j);
      }
    }
  }

  // allows no moves, call macroBoard.set after this is called
  private void restoreMacroBoard() {
    for (int i = 0; i < macroBoard.getRows(); i++) {
      for (int j = 0; j < macroBoard.getCols(); j++) {
        macroBoard.set(0, i, j);
      }
    }
  }
}
