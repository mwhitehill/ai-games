
public class Match {
	private String myPlayerName;
	private String theirPlayerName;
	private int myBotID;
	private int timeBank;
	private int timePerMove;

	private int currentRound;
	private int currentMove;
	private Board currentBoard;
	private Board currentMacroBoard;

	private Move lastMove;

	public String getMyPlayerName() {
		return myPlayerName;
	}
	public void setMyPlayerName(String myPlayerName) {
		this.myPlayerName = myPlayerName;
	}

	public String getTheirPlayerName() {
		return theirPlayerName;
	}
	public void setTheirPlayerName(String theirPlayerName) {
		this.theirPlayerName = theirPlayerName;
	}

	public int getMyBotID() {
		return myBotID;
	}
	public void setMyBotID(int myBotID) {
		this.myBotID = myBotID;
	}

	public int getTimeBank() {
		return timeBank;
	}
	public void setTimeBank(int timeBank) {
		this.timeBank = timeBank;
	}

	public int getTimePerMove() {
		return timePerMove;
	}
	public void setTimePerMove(int timePerMove) {
		this.timePerMove = timePerMove;
	}

	public int getCurrentRound() {
		return currentRound;
	}
	public void setCurrentRound(int currentRound) {
		this.currentRound = currentRound;
	}

	public int getCurrentMove() {
		return currentMove;
	}
	public void setCurrentMove(int currentMove) {
		this.currentMove = currentMove;
	}

	public Board getCurrentBoard() {
		return currentBoard;
	}

	/**
	 * Update the board with a given String holding the new board
	 * @param fieldVals
	 * 			the csv String representing the board
	 */
	public void setCurrentBoard(String fieldVals) {
		currentBoard = new Board(fieldVals);
	}

	public Board getCurrentMacroBoard() {
		return currentMacroBoard;
	}

	/**
	 * Update the macro board with a given String holding the new board
	 * @param fieldVals
	 * 			the csv String representing the macro board
	 */
	public void setCurrentMacroBoard(String fieldVals) {
		currentMacroBoard = new Board(fieldVals);
	}

	public Move getLastMove() {
		return lastMove;
	}

}
