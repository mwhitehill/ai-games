import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by michael on 2/20/16.
 */
public class Action {
    public static Move getMove(Match match){
        ArrayList<Move> possible=generatePossible(match.getCurrentMacroBoard(), match.getCurrentBoard(), match.getMyBotID());
        System.err.println("possible moves: "+possible);


        // FIXME: 2/20/16
        return null;
    }

    private static ArrayList<Move> generatePossible(Board macro, Board board, int playerID){
        ArrayList<Move>possibleMoves=new ArrayList<>(0);

        int macrorow=-1;
        int macrocol=-1;
        for(int row=0; row<macro.getRows(); row++){
            for(int col=0; col<macro.getCols(); col++){
                if(macro.get(row, col)==-1){
                    macrorow=row;
                    macrocol=col;
                    break;
                }
            }
            if(macrorow!=-1&&macrocol!=-1)
                break;
        }
        System.err.println("The macro board we're inspecting is: "+macrorow+", "+macrocol);

        Board playable=board.GetSubboard(macrorow,macrocol);

        for(int row=0; row<playable.getRows(); row++){
            for(int col=0; col<playable.getCols(); col++){
                if(playable.get(row, col)==0){
                    Move move=new Move(row+(macrorow*3), col+(macrocol*3), playerID);
                    possibleMoves.add(move);
                }

            }
        }
        return possibleMoves;
    }





}
